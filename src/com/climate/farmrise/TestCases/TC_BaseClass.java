package com.climate.farmrise.TestCases;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.climate.farmrise.ReusableUtils.GenericUtils;
import com.climate.farmrise.ReusableUtils.InitDriver;
import com.experitest.selenium.MobileWebDriver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class TC_BaseClass {

	private InitDriver objInitDriver = null;
	
	public static GenericUtils objGenericUtils = null;

	
	protected boolean actualResult = false;
	protected String actualMessage = null;
	protected Map<String, String> testDataMap = null;
	protected static MobileWebDriver driver = null;
	protected static String fileLocation = null;
	protected static ExtentReports objExtentReports = null;
	protected static ExtentTest objExtentTest = null;
	 

	@BeforeSuite
	void initTestSuite() {
		try {
			objInitDriver = new InitDriver();
			objGenericUtils = new GenericUtils();
			testDataMap = new HashMap<>();
			
			String[][] testData = objGenericUtils.readTestDataFromExcel(System.getProperty("user.dir") + "\\FarmRise_TestData.xlsx", "TestData");
			for(int i=0; i<testData.length;i++)
				testDataMap.put(testData[i][0], testData[i][1]);

			objInitDriver.initiateDriver("adb:SM-G900H");
			objGenericUtils.propetiesHandler(System.getProperty("user.dir") + "\\Properties\\ObjectRepository.properties");

			fileLocation = System.getProperty("user.dir") + "\\ExtentReports\\" + String.valueOf(new  Date().getTime()) + "\\";
			objExtentReports = new ExtentReports(fileLocation + "FarmriseTestExecutionReport.html");

			objExtentReports.addSystemInfo("OS", InitDriver.driver.client.getDeviceProperty("device.os") + "-" +  InitDriver.driver.client.getDeviceProperty("os.version"))
			.addSystemInfo("Environment", "Test Env");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@BeforeTest(alwaysRun=true)
	void initTestScenario(ITestContext refITestContext) {
		try {
			InitDriver.driver.client.launch("com.climate.farmrise/.SplashScreen", true, false);
			objExtentTest = objExtentReports.startTest(refITestContext.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterTest(alwaysRun=true)
	void tearDownTestScenario() {
		try {
			objExtentReports.endTest(objExtentTest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterSuite
	void tearDownTestSuite() {
		try {
			objInitDriver.endExecution();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			objExtentReports.flush();
			objExtentReports.close();
		}
	}
}

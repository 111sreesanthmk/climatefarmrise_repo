package com.climate.farmrise.TestCases;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.climate.farmrise.BusinessModules.HomePage;
import com.climate.farmrise.BusinessModules.WeatherPage;
import com.relevantcodes.extentreports.LogStatus;

public class TC_FarmRise_01 extends TC_BaseClass {

	private int hour = 0;
	private String clockHour = null;
	private List<String> testdata = null;

	private HomePage objHomePage = null;
	private WeatherPage objWeatherPage = null;

	@BeforeClass
	void prerequisites() {
		objHomePage = new HomePage();
		objWeatherPage = new WeatherPage();
	}

	@Test(retryAnalyzer=com.climate.farmrise.ReusableUtils.RetryFailedTests.class)
	void step01() {
		actualResult = objHomePage.tapOnHomeTab();
		actualMessage = "Home Tab is not displayed.";
		assertTrue(actualResult, actualMessage);		
		actualMessage = "Home Tab is displayed successfully.";
	}

	@Test(retryAnalyzer=com.climate.farmrise.ReusableUtils.RetryFailedTests.class, dependsOnMethods="step01")
	void step02() {
		actualResult = objHomePage.tapOnWeatherDetails();
		actualMessage = "Weather details screen is not displayed.";
		assertTrue(actualResult, actualMessage);		
		actualMessage = "Weather details screen displayed successfully.";
	}

	@Test(retryAnalyzer=com.climate.farmrise.ReusableUtils.RetryFailedTests.class, dependsOnMethods="step02", dataProvider = "timeVerification")
	void step03(String timeVerification) {
		actualResult = objWeatherPage.verifyTime(timeVerification);
		actualMessage = timeVerification + " is not displayed.";
		assertTrue(actualResult, actualMessage);		
		actualMessage = timeVerification + " is verified on the Horizontal scroll for timings.";
	}

	@DataProvider(name = "timeVerification")
	Object[] timeVerification() {
		try {
			testdata = new ArrayList<>();
			testdata.add("Now");

			hour = java.time.LocalTime.now().getHour();
			hour++;
			if(hour>12) {
				hour = hour-12;
				clockHour = " PM";
			}
			else
				clockHour = " AM";

			for(int i=1; i<24 ; i++) {
				if(hour > 12) {
					hour = 1;
					if(clockHour==" AM")
						clockHour = " PM";
					else
						clockHour = " AM";
				}
				testdata.add(String.valueOf(hour+clockHour));
				hour++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return testdata.toArray();
	}

	@AfterMethod
	void testResultsAnalyzer(ITestResult refITestResult) {
		try {
			String imageLocation = fileLocation + refITestResult.getName() + "_"  + String.valueOf(new  Date().getTime());
			objGenericUtils.captureScreenshot(imageLocation);

			if (ITestResult.SUCCESS == refITestResult.getStatus()) {
				objExtentTest.log(LogStatus.PASS, refITestResult.getName(),
						actualMessage + objExtentTest.addScreenCapture(imageLocation + ".jpeg"));
			}
			if (ITestResult.FAILURE == refITestResult.getStatus()) {
				objExtentTest.log(LogStatus.FAIL, refITestResult.getName(),
						actualMessage + objExtentTest.addScreenCapture(imageLocation + ".jpeg"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

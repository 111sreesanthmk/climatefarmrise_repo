package com.climate.farmrise.TestCases;

import static org.testng.Assert.assertTrue;

import java.util.Date;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.climate.farmrise.BusinessModules.GovernmentSchemes;
import com.climate.farmrise.BusinessModules.HomePage;
import com.relevantcodes.extentreports.LogStatus;

public class TC_FarmRise_02 extends TC_BaseClass {

	private HomePage objHomePage = null;
	private GovernmentSchemes objGovernmentSchemes = null;
	
	@BeforeClass
	void prerequisites() {
		objHomePage = new HomePage();
		objGovernmentSchemes = new GovernmentSchemes();
	}

	@Test(retryAnalyzer=com.climate.farmrise.ReusableUtils.RetryFailedTests.class)
	void step01() {
		actualResult = objHomePage.tapOnMoreTab();
		actualMessage = "More Tab is not displayed.";
		assertTrue(actualResult, actualMessage);		
		actualMessage = "More Tab is displayed successfully.";
	}

	@Test(retryAnalyzer=com.climate.farmrise.ReusableUtils.RetryFailedTests.class, dependsOnMethods="step01")
	void step02() {
		actualResult = objHomePage.tapOnGovermentSchemes();
		actualMessage = "Weather details screen is not displayed.";
		assertTrue(actualResult, actualMessage);		
		actualMessage = "Weather details screen displayed successfully.";
	}

	@Test(retryAnalyzer=com.climate.farmrise.ReusableUtils.RetryFailedTests.class, dependsOnMethods="step02")
	void step03() {
		actualResult = objGovernmentSchemes.scrollDown(3);
		actualMessage =  "Did not scroll down three times.";
		assertTrue(actualResult, actualMessage);		
		actualMessage = "Scrolled down three times in Goverment scheme page.";
		
		actualResult = objGovernmentSchemes.verifyLoadMoreSchemeButton();
		actualMessage =  "Load More scheme button is not displayed.";
		assertTrue(actualResult, actualMessage);		
		actualMessage = "Load More scheme button is displayed.";
	}

	@Test(retryAnalyzer=com.climate.farmrise.ReusableUtils.RetryFailedTests.class, dependsOnMethods="step03", dataProvider="searchKey")
	void step04(String searchKey) {
		actualResult = objGovernmentSchemes.searchScheme(searchKey);
		actualMessage =  "Did not search for '" + searchKey + "'.";
		assertTrue(actualResult, actualMessage);		
		actualMessage = "Searched for '" + searchKey + "'.";
		
		actualResult = objGovernmentSchemes.verifySearchResults();
		actualMessage =  "No results displayed for search query '" + searchKey + ".";
		assertTrue(actualResult, actualMessage);		
		actualMessage = "Search results displayed.";
	}
	
	@DataProvider(name = "searchKey")
	Object[] searchKey() {
		return new String[] {testDataMap.get("Searchkey")};
	}
	
	@AfterMethod
	void testResultsAnalyzer(ITestResult refITestResult) {
		try {
			String imageLocation = fileLocation + refITestResult.getName() + "_"  + String.valueOf(new  Date().getTime());
			objGenericUtils.captureScreenshot(imageLocation);
			
			if (ITestResult.SUCCESS == refITestResult.getStatus()) {
				objExtentTest.log(LogStatus.PASS, refITestResult.getName(),
						actualMessage + objExtentTest.addScreenCapture(imageLocation + ".jpeg"));
			}
			if (ITestResult.FAILURE == refITestResult.getStatus()) {
				objExtentTest.log(LogStatus.FAIL, refITestResult.getName(),
						actualMessage + objExtentTest.addScreenCapture(imageLocation + ".jpeg"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

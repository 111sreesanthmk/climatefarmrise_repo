package com.climate.farmrise.BusinessModules;

import com.climate.farmrise.ReusableUtils.InitDriver;
import com.climate.farmrise.ReusableUtils.PageUtils;
import com.climate.farmrise.TestCases.TC_BaseClass;

public class GovernmentSchemes extends BusinessModules {

	private boolean flag = false;
	private PageUtils objPageUtils = null;

	public GovernmentSchemes() {
		objPageUtils = new PageUtils();
	}
	
	@Override
	public boolean scrollDown(int count) {
		int swipeOffset = objPageUtils.getElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("govermentSchemesSection")).getLocation().getY();
		int rep = 0;
		try {
		while(rep<=count) {
			InitDriver.driver.client.swipe("Down", swipeOffset, 500);
			flag = true;
			rep++;
		}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public boolean verifyLoadMoreSchemeButton() {
		return objPageUtils.verifyElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("verifyLoadMoreScheme_Btn"));
	}

	@Override
	public boolean searchScheme(String scheme) {
		if(objPageUtils.clickElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("searchIcon")))
			objPageUtils.getElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("searchText")).sendKeys(scheme);
		return objPageUtils.clickElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("searchIcon"));
	}
	
	@Override
	public boolean verifySearchResults() {
		return objPageUtils.verifyElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("searchResults"));
	}
	
}

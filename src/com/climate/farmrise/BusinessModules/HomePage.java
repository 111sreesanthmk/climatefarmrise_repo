package com.climate.farmrise.BusinessModules;

import com.climate.farmrise.ReusableUtils.PageUtils;
import com.climate.farmrise.TestCases.TC_BaseClass;

public class HomePage extends BusinessModules {

	private PageUtils objPageUtils = null;

	public HomePage() {
		objPageUtils = new PageUtils();
	}

	@Override
	public boolean tapOnHomeTab() {
		if(!objPageUtils.getElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("toolBarTitle")).getText().equalsIgnoreCase("Home"))
			objPageUtils.clickElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("homeTab"));
		return objPageUtils.getElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("toolBarTitle")).getText().equalsIgnoreCase("Home");
	}

	@Override
	public boolean tapOnMoreTab() {
		if(!objPageUtils.getElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("toolBarTitle")).getText().equalsIgnoreCase("More"))
			objPageUtils.clickElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("moreTab"));
		return objPageUtils.getElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("toolBarTitle")).getText().equalsIgnoreCase("More");
	}

	@Override
	public boolean tapOnWeatherDetails() {
		if(!objPageUtils.getElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("toolBarTitle")).getText().equalsIgnoreCase("Weather"))
			objPageUtils.clickElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("accessWeatherDetailsUnderWeatherUpdate"));
		return objPageUtils.getElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("toolBarTitle")).getText().equalsIgnoreCase("Weather");
	}
	
	@Override
	public boolean tapOnGovermentSchemes() {
		if(!objPageUtils.getElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("toolBarTitle")).getText().equalsIgnoreCase("Government Schemes"))
			objPageUtils.clickElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("govermentSchemesOption"));
		return objPageUtils.getElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("toolBarTitle")).getText().equalsIgnoreCase("Government Schemes");
	}

	


}

package com.climate.farmrise.BusinessModules;

import com.climate.farmrise.ReusableUtils.InitDriver;
import com.climate.farmrise.ReusableUtils.PageUtils;
import com.climate.farmrise.TestCases.TC_BaseClass;

public class WeatherPage extends BusinessModules {

	private boolean flag = false;
	private PageUtils objPageUtils = null;

	public WeatherPage() {
		objPageUtils = new PageUtils();
	}
	
	@Override
	public boolean verifyTime(String time) {
		flag  = objPageUtils.verifyElementByXpath(TC_BaseClass.objGenericUtils.propertyValue("weatherTiming") + "[text()='" + time +"']");
		InitDriver.driver.client.elementSwipe("NATIVE", TC_BaseClass.objGenericUtils.propertyValue("weatherTiming") + "[text()='" + time +"']", 0, "Right", 0, 0);  
		return flag;
	}

}

package com.climate.farmrise.BusinessModules;

public abstract class BusinessModules {

	// Author:	 	Sreesanth
	// Created On:	May 07, 2019
	// Updated by:	Sreesanth
	// Updated On:	May 07, 2019
	// Description:	Checks Home page is displayed, if false, then click on the home tab and confirms home page is displayed.  
	public boolean tapOnHomeTab() {
		return false;
	}

	// Author:		Sreesanth
	// Created On: 	May 07, 2019
	// Updated by: 	Sreesanth
	// Updated On:	May 07, 2019
	// Description:	Checks More page is displayed, if false, then click on the More tab and confirms More page is displayed.
	public boolean tapOnMoreTab() {
		return false;
	}

	// Author:	 	Sreesanth
	// Created On:	May 09, 2019
	// Updated by:	Sreesanth
	// Updated On:	May 09, 2019
	// Description:	Checks weather page is displayed, if false, then click on Access weather details on home page and confirms weather page is displayed.
	public boolean tapOnWeatherDetails()  {
		return false;
	}

	// Author:	 	Sreesanth
	// Created On:	May 09, 2019
	// Updated by:	Sreesanth
	// Updated On:	May 09, 2019
	// Description:	verify time is displayed in the weather screen, which is passed as a paramter 
	public boolean verifyTime(String time) {
		return false;
	}

	// Author:	 	Sreesanth
	// Created On:	May 09, 2019
	// Updated by:	Sreesanth
	// Updated On:	May 09, 2019
	// Description:	Checks Government Schemes page is displayed, if false, then click on Government Schemes on more page and confirms Government Schemes page is displayed.
	public boolean tapOnGovermentSchemes() {
		return false;
	}

	// Author:	 	Sreesanth
	// Created On:	May 13, 2019
	// Updated by:	Sreesanth
	// Updated On:	May 13, 2019
	// Description:	swipe in up ward direction as per the number passed as paramter
	public boolean scrollDown(int count) {
		// TODO Auto-generated method stub
		return false;
	}

	// Author:	 	Sreesanth
	// Created On:	May 13, 2019
	// Updated by:	Sreesanth
	// Updated On:	May 13, 2019
	// Description:	Verifies Load More Scheme button is displayed in Government Scheme screen
	public boolean verifyLoadMoreSchemeButton() {
		return false;
	}

	// Author:	 	Sreesanth
	// Created On:	May 13, 2019
	// Updated by:	Sreesanth
	// Updated On:	May 13, 2019
	// Description:	Clicks on search icon in the Government scheme screen, enters search key available in scheme and clicks on search icon again  
	public boolean searchScheme(String scheme) {
		return false;
	}
	
	// Author:	 	Sreesanth
	// Created On:	May 13, 2019
	// Updated by:	Sreesanth
	// Updated On:	May 13, 2019
	// Description:	Verify search key returns results   
	public boolean verifySearchResults() {
		return false;
	}

}

package com.climate.farmrise.ReusableUtils;

import com.experitest.selenium.MobileWebDriver;

public class InitDriver {

	public static MobileWebDriver driver = null; 

	public void initiateDriver(String deviceName) {
		try {
			driver = new MobileWebDriver("localhost", 8889, System.getProperty("user.dir") + "\\Properties\\Repository", "xml", System.getProperty("user.dir") + "/Hybris_Reports", "FarmRise");
			setDevice(deviceName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setDevice(String deviceName) {
		try {
			driver.setDevice(deviceName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void endExecution() {
		try {
			driver.releaseClient();
			driver.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

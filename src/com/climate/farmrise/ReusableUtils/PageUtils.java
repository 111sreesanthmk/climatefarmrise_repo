package com.climate.farmrise.ReusableUtils;

import org.openqa.selenium.By;

import com.experitest.selenium.MobileWebElement;

public class PageUtils {

	private boolean flag = false;
	private final int objectWaitTime = 30000;
	private MobileWebElement element = null;
	
	public MobileWebElement getElementByXpath(String xpath) {
		try {
			if (xpath != null) {
				InitDriver.driver.findElement(By.xpath(xpath)).waitFor(objectWaitTime);
				element = InitDriver.driver.findElement(By.xpath(xpath));
			}
		} catch (Exception e) {
			e.printStackTrace();
			element = null;
		}
		return element;
	}

	public boolean clickElementByXpath(String xpath) {

		try {
			if (xpath != null) {
				getElementByXpath(xpath).click();
				flag = true;
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	public boolean verifyElementByXpath(String xpath) {

		try {
			if (xpath != null) {
				getElementByXpath(xpath).verify();
				flag = true;
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

}
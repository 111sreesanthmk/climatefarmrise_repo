package com.climate.farmrise.ReusableUtils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import jsystem.utils.FileUtils;

public class GenericUtils {

	private static Properties objProperties = null;

	public boolean propetiesHandler(String fileLocation) {
		try {
			objProperties = new Properties();
			objProperties.load(new FileInputStream(fileLocation));
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String propertyValue(String propertyKey) {
		return objProperties.getProperty(propertyKey);
	}

	public void captureScreenshot(String imageLocation) {
		try {
			File scrFile = ((TakesScreenshot) InitDriver.driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(imageLocation + ".jpeg"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String[][] readTestDataFromExcel(String path, String sheetName) throws Exception {

		int i = 0;
		int j = 0;
		String[][] value = null;
		FileInputStream fis = new FileInputStream(path);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFRow row = null;
		XSSFCell cell = null;
		Iterator<Cell> cellIterator = null;

		try {
			if(!isSheetExists(workbook, sheetName)) {
				throw new Exception("'" + sheetName + "' sheet not available");	
			}

			XSSFSheet sh =  workbook.getSheet(sheetName);
			Iterator<Row> rowIterator = sh.rowIterator();

			value = new String[sh.getLastRowNum()+1][sh.getRow(0).getLastCellNum()];

			while(rowIterator.hasNext()) {
				row = (XSSFRow) rowIterator.next();
				cellIterator = row.cellIterator();
				while(cellIterator.hasNext()) {
					cell = (XSSFCell) cellIterator.next();

					switch(cell.getCellType()) {
					case 0:
						value[i][j] = String.valueOf(cell.getNumericCellValue());
						break;
					case 1:
						value[i][j] = cell.getStringCellValue();
						break;
					case 2:
						value[i][j] = String.valueOf(cell.getRichStringCellValue());
						break;
					default:
						value[i][j] = "";
						break;
					}
					j++;
				}
				i++;
				j=0;
			}
		}
		catch(Throwable e) {
			e.printStackTrace();
		}

		return value;
	}

	private boolean isSheetExists(XSSFWorkbook workbook, String sheetName)  {

		boolean flag = false;
		try {
			for(int i=0; i<workbook.getNumberOfSheets();i++) {
				if(workbook.getSheetAt(i).getSheetName().equalsIgnoreCase(sheetName)) {
					flag = true;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		return flag; 
	}
}

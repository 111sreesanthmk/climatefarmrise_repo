package com.climate.farmrise.ReusableUtils;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryFailedTests implements IRetryAnalyzer {

	private int retryinit = 0;
	private int retryMax = 2;

	@Override
	public boolean retry(ITestResult result) {
		if (!result.isSuccess()) {            
			if (retryinit < retryMax) {   
				retryinit++;
				return true;
			}
		}
		return false;
	}
}

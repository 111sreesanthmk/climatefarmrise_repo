package com.climate.farmrise.PageObjects;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.climate.farmrise.ReusableUtils.InitDriver;
import com.experitest.selenium.MobileWebElement;

public abstract class HomePage {

	
	
	@FindBy(how = How.ID, using = "action_home")
	protected MobileWebElement HomeTab;
	
	@FindBy(how = How.ID, using = "action_more")
	protected MobileWebElement MoreTab;
	
	
	public HomePage() {
		PageFactory.initElements(InitDriver.driver, com.climate.farmrise.PageObjects.HomePage.class);
	}
	
}
